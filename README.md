# λ’Coding in draft

PDF: https://gitlab.com/minimalprocedure/lambda_coding  
Code: https://gitlab.com/minimalprocedure/lcodc

Questo testo si potrebbe _risolvere velocemente_ e alla domanda a chi sia rivolto non c'è nessuna risposta e se voglia essere un corso di programmazione nemmeno.  

Andrebbe preso semplicemente, come una serie di _spunti_ e informazioni più o meno _frammentarie_ su un mondo, quello della programmazione, considerato troppo spesso oltremodo complesso o decisamente troppo semplice.     
Per questo motivo non è così facile identificare un vero _target_ di potenziali lettori e provarci andrebbe oltre i miei possibili meriti. Quindi non è né per eventuali insegnanti e né per studenti, ma solo per chi possa essere almeno per un minimo _curioso_.     

Affrontare in maniera sistematica l'argomento potrebbe essere oggetto di un'intera enciclopedia, quella che qualcuno ricorderà veniva venduta porta a porta da signori gioviali in giacca grigia e cravatta sgargiante. Come dire... Troppo tempo e troppa fatica.      
I programmatori sono _gente pigra_ che parla di cose strane e quindi come mi si conviene nel ruolo, ho cercato saltellando qua e là di allestire delle piccole finestre verso questo _strano_ mondo da _tecnici_ (ma non chiamateci _tecnici_ per favore, che non sappiamo riparare i videoregistratori). Starà ad altri aprirle.     

Dimenticavo... Meno che mai vorrei fare del _coding_.      

Il testo è diviso in alcune parti ma raggruppabili in due principali. Nella prima si presenteranno alcuni degli strumenti con cui solitamente chi programma ha a che fare. È una scelta puramente arbitraria e dettata da preferenze strettamente personali nonché infarcita di _nomi_, che almeno nelle scuole si tenderà a non incontrare. Lo scopo sarebbe quello di instillare nel potenziale lettore una _sana curiosità_ dell'_altro da sé_ e spingerlo ad almeno provare l'alternativa.

Molti di questi strumenti inoltre, potrebbero essere utilizzati anche nella _vita digitale_ di tutti i giorni: per scrivere, mantenere uno storico di ciò che si è prodotto e così via. Il testo qui presente, è stato scritto con l'editor _Emacs_ e con una sua estensione chiamata _Org mode_, che permette da un unico _sorgente_ di generare altri formati di documento come, per esempio, il pdf.

Nella seconda invece si affronterà un vero e proprio _linguaggio di programmazione_. Uno di quelli veri e di quelli che nessuno si sognerebbe, forse mai, di insegnare in una scuola superiore: niente _blocchetti colorati_ ma anche niente _Python_ (niente di personale contro il _Python_) e né _Java_ o _C++_. Volendo, avrei potuto affrontate l'impresa con _Kojo_, avendo partecipato anche se in una minima (minima!) parte al suo sviluppo o con il mio piccolo ambiente per sperimentare in _Ruby_: _DuckQuack_.

Non sarà così.

  Che posso dire a mia discolpa? Per Kojo c'è un altro libro interessante dell'amico Stefano Penge[fn:coding_creativita] e quindi sarei arrivato comunque tardi e per _DuckQuack_ invece, non ho una vera risposta.      
  _DuckQuack_ è nato quasi per sfida e per dimostrare che se gli ambienti per _insegnare la programmazione_ non ci sono o sono _abbandonati_, con un pizzico di buona volontà si potrebbero sempre fare o adattare. Lo consideriamo _pensiero computazionale_? Se non altro è _problem solving_.     

  Gli esempi proposti, i progetti, avranno un taglio definibile come _educativo_ ma non nel senso del codice _perfetto_ o _performante_ e _bello_. A sguardo _esperto_ saranno sicuramente poco o molto _artigianali_, ma cercheranno di trasmettere al lettore i principi di base della _programmazione funzionale_ senza troppo attingere a _librerie_ interne o esterne e talvolta _reinventando la ruota_. Sicuramente molti altri avrebbero potuto scrivere di meglio e se qualche lettore arriverà al punto di _criticare_ questi codici, avranno comunque e forse anche di più, raggiunto il loro scopo. Il _programmare_ deve, non dovrebbe, essere un mettersi in discussione continuamente accettando umilmente gli eventuali propri errori. Ci sarà sempre qualcuno che ne saprà più di noi o avrà trovato _strade_ più interessanti.     
  È un imparare continuo e questo è forse ciò che lo rende _affascinante_.     

  Per chiudere senza farla troppo lunga, non sarà certamente un percorso lineare da _libro che insegna un linguaggio di programmazione_, quanto piuttosto una sorta di _confusionario progetto perennemente in beta_: come direbbe un programmatore di computer.       

  Prendetelo per quello che è e _senza pretese_.
  
--------------------------------------------------------------------------------
Warning, the text is in Italian! :D
--------------------------------------------------------------------------------

# λ’Coding in draft

This textbook could be _resolved quickly_ and to the question to whom it's addressed there is no a real answer and neither if it wants to be a programming course.

It should be taken simply, as a series of _spots_, more or less a _fragmentary_ information, on _the world, of programming_, too often considered very complex or too simple.
For this reason it's not so easy to identify a true potential readers _target_, and trying would be beyond my possible merits. So it's neither for teachers nor for students, only for those who may be, at least for a minimum, _nosey_.

A systematic way to address this argument could be the subject of an full encyclopedia, the one that some will remember was being sold by a door-to-door jovial gentlemen in gray jackets and flamboyant ties. How say ... Too much time and too much effort.
Programmers are a very _lazy agent_ who talks about strange things and therefore, as it suits me in the role, I tried to hop here and there opening some small windows towards this _strange_ world by _technicians_ (but don't call us _technicians_ please, we don't know how to fix VCRs ). It will be up to others to open them.

I forgot... Least of all I would like to do some _coding_.

Text is divided into some parts, but this can be grouped into two main ones. In the first, will be presented some of the tools that the programmer usually deals with. It's a purely arbitrary choice and dictated by strictly personal preferences as well as filled with _names_, which at least in schools you'll tend not to meet. The aim would be to instill in the potential reader a _healthy curiosity_ about the _other than himself_ and push him to try the alternative.

Furthermore, many of these tools could also be used in everyday _digital life_: to write, keep a history of what has been produced and so on. The text herein was written with the _Emacs_ editor and with an extension called _Org mode_, which allows you to generate other document formats from a single _source_, such as, for example, a pdf.

In the second, on the other hand, a real _programming language_ will be dealt with. One of the real ones and the ones that no one would dream, perhaps ever, of teaching in a high school: no _colored blocks_ but also nothing _Python_ (nothing personal against _Python_) and neither _Java_ or _C++_. If I wanted, I could have tackled the feat with _Kojo_, having participated even if in a minimal (minimal!) part in its development or with my own little environment to experiment in _Ruby_: _DuckQuack_.

It will not be so.

  What can I say in my defense? For _Kojo_ there is another interesting book in italian by my friend Stefano Penge and therefore too late I arrived anyway, for _DuckQuack_ instead, I don't have a real answer.
  _DuckQuack_ was born almost as a challenge and to demonstrate that if the environments for _teaching programming_ dont exist or are _abandoned_, with a _pinch of good_ will they could always be done or adapted. Do we consider it _ computational thinking_? If nothing else, it's almost a _problem solving_.

  The proposed examples, the projects, have a slant that can be defined as _educational_ but not in the sense of the _perfect_ or _performing_ and _beautiful_ code. At an _expert look_, they will certainly be little or very _naive_, but try to convey the reader to the basic principles of _functional programming_ without drawing too much on internal or external _libraries_ and sometimes _reinventing the wheel_. Surely many others could have written better and if any reader gets to the point of _critifying_ these codes, they will have achieved the purpose anyway and perhaps even more. _The act of programming_ must be a continually questioning oneself and humbly accepting one's own mistakes. There will always be someone who will know more of ourself or will have found _ways_ more interesting.
  It is a continuous learning and this is perhaps what makes it so _fascinating_.

  To close without making too long, it will certainly not be a linear path by a _book that teaches a programming language_, but rather a sort of _confusing project perpetually in beta_ as a computer programmer would say.

Take it for what it is. 


